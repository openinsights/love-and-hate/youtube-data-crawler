from types import SimpleNamespace


def load_app_config(config_parser, cli_config):
    channel_batch_size = int(config_parser['APP']['CHANNEL_BATCH_SIZE'])
    video_batch_size = int(config_parser['APP']['VIDEO_BATCH_SIZE'])
    comment_batch_size = int(config_parser['APP']['COMMENT_BATCH_SIZE'])

    if cli_config.channel_limit is None:
        channel_limit = int(config_parser['APP']['CHANNEL_LIMIT'])
    else:
        channel_limit = cli_config.channel_limit
    if cli_config.video_limit is None:
        video_limit = int(config_parser['APP']['VIDEO_LIMIT'])
    else:
        video_limit = cli_config.video_limit
    if cli_config.comment_limit is None:
        comment_limit = int(config_parser['APP']['COMMENT_LIMIT'])
    else:
        comment_limit = cli_config.comment_limit
    if cli_config.update_interval is None:
        update_interval = int(config_parser['APP']['UPDATE_INTERVAL'])
    else:
        update_interval = cli_config.update_interval

    conf = {
        'channel_limit': channel_limit,
        'channel_batch_size': channel_batch_size,
        'video_limit': video_limit,
        'video_batch_size': video_batch_size,
        'comment_limit': comment_limit,
        'comment_batch_size': comment_batch_size,
        'update_interval': update_interval,
    }
    return SimpleNamespace(**conf)


def handle_cli_arguments(argument_parser):
    parser = argument_parser(
        description="Fetch it, bring it, babe, watch it, turn it, leave it, stop, format it"
    )
    mode = parser.add_argument_group('MODE', 'set mode for the operation')
    mode = mode.add_mutually_exclusive_group(required=True)
    mode.add_argument(
        "-v",
        "--validate",
        action="store_true",
        help="validate entries with youtube id value only",
    )
    mode.add_argument(
        "-u",
        "--update",
        action="store_true",
        help="update data older than the specified interval",
    )
    mode.add_argument(
        "-f",
        "--fetch",
        action="store_true",
        help="fetch new data from target resource",
    )
    mode.add_argument(
        "-i",
        "--import-file",
        help="import trusted data from csv file",
    )
    target = parser.add_argument_group(
        'TARGET',
        'set target resource'
    )
    target = target.add_mutually_exclusive_group(required=True)
    target.add_argument(
        "--channels",
        action="store_true"
    )
    target.add_argument(
        "--videos",
        action="store_true"
    )
    target.add_argument(
        "--comments",
        action="store_true"
    )
    target.add_argument(
        "--video-ids",
        action="store_true",
        help="fetch video ids from channels uploads"
    )
    target.add_argument(
        "--video-details",
        action="store_true",
        help="fetch details for video ids"
    )

    params = parser.add_argument_group(
        'CONFIG',
        'set parameters to overwrite file config'
    )
    params.add_argument(
        '--channel-limit',
        type=int,
        help='set channel limit for operation'
    )
    params.add_argument(
        '--video-limit',
        type=int,
        help='set limit for videos per channel'
    )
    params.add_argument(
        '--comment-limit',
        type=int,
        help='set limit for comments per video'
    )
    params.add_argument(
        '--update-interval',
        type=int,
        help='sets allowed data age in days'
    )

    return parser
