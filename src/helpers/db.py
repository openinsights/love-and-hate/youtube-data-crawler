from datetime import datetime, timezone, date
from psycopg2.extensions import AsIs
import psycopg2


def update_channel(db_conn, channel):
    db_cur = db_conn.cursor()
    now = datetime.now(timezone.utc)
    channel['updated_at'] = now
    columns = list(channel.keys())
    values = [channel[column] for column in columns]
    query = "UPDATE channels SET (%s) = (%s) WHERE channel_id = %s;"
    db_cur.execute(query, (AsIs(','.join(columns)), tuple(values), channel['channel_id']))
    db_cur.close()


def insert_video(db_conn, video):
    db_cur = db_conn.cursor()
    now = datetime.now(timezone.utc)
    video['created_at'] = now
    video['updated_at'] = now
    columns = list(video.keys())
    values = [video[column] for column in columns]
    query = "INSERT INTO videos (%s) VALUES %s ON CONFLICT DO NOTHING;"
    db_cur.execute(query, (AsIs(','.join(columns)), tuple(values),))
    db_cur.close()


def update_video(db_conn, video):
    db_cur = db_conn.cursor()
    now = datetime.now(timezone.utc)
    video['updated_at'] = now
    columns = list(video.keys())
    values = [video[column] for column in columns]
    query = "UPDATE videos SET (%s) = (%s) WHERE video_id = %s;"
    db_cur.execute(query, (AsIs(','.join(columns)), tuple(values), video['video_id']))
    db_cur.close()


def insert_row(db_conn, query, values):
    cursor = db_conn.cursor()
    try:
        cursor.execute(query, values)
        db_conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        print("Error: %s" % error)
        db_conn.rollback()
        cursor.close()
        return 1
    cursor.close()


def insert_quota_usage(db_conn, helpers):
    db_cur = db_conn.cursor()
    date = helpers.get_api_date()

    query = """
        INSERT INTO quota_usage (api_date, quotas)
        VALUES (%s, %s)
        ON CONFLICT (api_date) DO UPDATE
        SET quotas = quota_usage.quotas + %s;
    """
    db_cur.execute(query, (date, helpers.quota_usage, helpers.quota_usage,))
    db_conn.commit()
    db_cur.close()


def insert_comment(db_conn, comment):
    db_cur = db_conn.cursor()
    now = datetime.now(timezone.utc)
    comment['created_at'] = now
    comment['updated_at'] = now
    columns = list(comment.keys())
    values = [comment[column] for column in columns]
    query = "INSERT INTO comments (%s) VALUES %s ON CONFLICT DO NOTHING;"
    db_cur.execute(query, (AsIs(','.join(columns)), tuple(values),))
    db_cur.close()


def update_video_count(db_conn, channel_id):
    now = datetime.now(timezone.utc)
    db_cur = db_conn.cursor()
    query = """
        SELECT COUNT(*)
        FROM videos
        WHERE channel_id = %s;
    """
    db_cur.execute(query, (channel_id,))
    video_count = db_cur.fetchone()
    query = """
        UPDATE channels
        SET video_count = %s,
            updated_at = %s
        WHERE channel_id = %s;
    """
    db_cur.execute(query, (video_count, now, channel_id,))
    db_conn.commit()
    db_cur.close()


def update_comment_count(db_conn, video_id):
    now = datetime.now(timezone.utc)
    db_cur = db_conn.cursor()
    query = """
        SELECT COUNT(*)
        FROM comments
        WHERE video_id = %s;
    """
    db_cur.execute(query, (video_id,))
    comment_count = db_cur.fetchone()
    query = """
        UPDATE videos
        SET comment_count = %s,
            updated_at = %s
        WHERE video_id = %s;
    """
    db_cur.execute(query, (comment_count, now, video_id,))
    db_conn.commit()
    db_cur.close()


def update_comments_disabled(db_conn, video_id, status):
    now = datetime.now(timezone.utc)
    db_cur = db_conn.cursor()
    query = """
            UPDATE videos
            SET comments_disabled = %s,
                updated_at = %s
            WHERE video_id = %s;
        """
    db_cur.execute(query, (status, now, video_id,))
    db_conn.commit()
    db_cur.close()
