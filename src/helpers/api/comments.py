
from helpers.api.common import increment_quota_count


def fetch_comment_threads(youtube, video_id, page_token=""):
    request = youtube.commentThreads().list(
        part="snippet,replies",
        videoId=video_id,
        order="relevance",
        pageToken=page_token,
        maxResults=100
    )
    increment_quota_count()
    response = request.execute()
    return response


def comment_thread_objs_from_response(response):
    comments = list()
    for item in response["items"]:
        comment = {}
        try:
            comment["comment_id"] = item["snippet"]["topLevelComment"]["id"]
        except KeyError:
            # drop if not present
            continue
        try:
            comment["video_id"] = item["snippet"]["videoId"]
        except KeyError:
            # drop if not present
            continue
        try:
            comment["comment_thread_etag"] = item["etag"]
        except KeyError:
            # drop if not present
            continue
        try:
            comment["comment_thread_id"] = item["id"]
        except KeyError:
            # drop if not present
            continue
        try:
            comment["reply_count"] = item["snippet"]["totalReplyCount"]
        except KeyError:
            # drop if not present
            continue
        try:
            comment["author_display_name"] = item["snippet"]["topLevelComment"]["snippet"]["authorDisplayName"]
        except KeyError:
            # drop if not present
            continue
        try:
            comment["author_channel_id"] = item["snippet"]["topLevelComment"]["snippet"]["authorChannelId"]["value"]
        except KeyError:
            # drop if not present
            continue
        try:
            comment["text_display"] = item["snippet"]["topLevelComment"]["snippet"]["textDisplay"]
        except KeyError:
            # drop if not present
            continue
        try:
            comment["text_original"] = item["snippet"]["topLevelComment"]["snippet"]["textOriginal"]
        except KeyError:
            # drop if not present
            continue
        try:
            comment["like_count"] = item["snippet"]["topLevelComment"]["snippet"]["likeCount"]
        except KeyError:
            # drop if not present
            continue
        try:
            comment["published_at"] = item["snippet"]["topLevelComment"]["snippet"]["publishedAt"]
        except KeyError:
            # drop if not present
            continue
        try:
            comment["edited_at"] = item["snippet"]["topLevelComment"]["snippet"]["updatedAt"]
        except KeyError:
            # drop if not present
            continue
        comments.append(comment)

        if "replies" in item:
            for reply in item["replies"]["comments"]:
                rep_comm = {}
                try:
                    rep_comm["comment_id"] = reply["id"]
                except KeyError:
                    # drop if not present
                    continue
                try:
                    rep_comm["video_id"] = reply["snippet"]["videoId"]
                except KeyError:
                    # drop if not present
                    continue
                try:
                    rep_comm["comment_thread_etag"] = item["etag"]
                except KeyError:
                    # drop if not present
                    continue
                try:
                    rep_comm["comment_thread_id"] = item["id"]
                except KeyError:
                    # drop if not present
                    continue
                try:
                    rep_comm["author_display_name"] = reply["snippet"]["authorDisplayName"]
                except KeyError:
                    # drop if not present
                    continue
                try:
                    rep_comm["author_channel_id"] = reply["snippet"]["authorChannelId"]["value"]
                except KeyError:
                    # drop if not present
                    continue
                try:
                    rep_comm["text_display"] = reply["snippet"]["textDisplay"]
                except KeyError:
                    # drop if not present
                    continue
                try:
                    rep_comm["text_original"] = reply["snippet"]["textOriginal"]
                except KeyError:
                    # drop if not present
                    continue
                try:
                    rep_comm["parent_id"] = reply["snippet"]["parentId"]
                except KeyError:
                    # drop if not present
                    continue
                try:
                    rep_comm["like_count"] = reply["snippet"]["likeCount"]
                except KeyError:
                    # drop if not present
                    continue
                try:
                    rep_comm["published_at"] = reply["snippet"]["publishedAt"]
                except KeyError:
                    # drop if not present
                    continue
                try:
                    rep_comm["edited_at"] = reply["snippet"]["updatedAt"]
                except KeyError:
                    # drop if not present
                    continue

                comments.append(rep_comm)

    return comments
