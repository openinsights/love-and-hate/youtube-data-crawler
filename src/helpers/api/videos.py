
from helpers.api.common import increment_quota_count


def fetch_playlist_items(youtube, playlist_id, page_token=""):
    request = youtube.playlistItems().list(
        part="snippet,contentDetails",
        playlistId=playlist_id,
        pageToken=page_token,
        maxResults=50
    )
    increment_quota_count()
    response = request.execute()
    return response


def playlistItem_objs_from_response(response):
    playlist_items = list()
    for item in response["items"]:
        playlist_item = {}
        playlist_item["kind"] = item["kind"]
        playlist_item["etag"] = item["etag"]
        playlist_item["published_at"] = item["snippet"]["publishedAt"]
        try:
            playlist_item["channel_id"] = item["snippet"]["videoOwnerChannelId"]
        except KeyError:
            playlist_item["channel_id"] = item["snippet"]["channelId"]

        playlist_item["title"] = item["snippet"]["title"]
        playlist_item["description"] = item["snippet"]["description"]
        try:
            playlist_item["video_id"] = item["contentDetails"]["videoId"]
        except KeyError:
            # drop if no video id present
            continue
        try:
            playlist_item["published_at"] = item["contentDetails"]["videoPublishedAt"]
        except KeyError:
            # drop if not present
            continue
        playlist_items.append(playlist_item)
    return playlist_items


def fetchVideoDetails(youtube, video_ids):
    request = youtube.videos().list(
        part="snippet,contentDetails,status,statistics,topicDetails",
        id=video_ids,
        hl="en_US",
        maxResults=50
    )
    increment_quota_count()
    response = request.execute()
    return response


def video_objs_from_response(response):
    videos = list()
    for item in response["items"]:
        video = {}
        video["kind"] = item["kind"]
        video["etag"] = item["etag"]
        video["video_id"] = item["id"]
        try:
            video["published_at"] = item["snippet"]["publishedAt"]
        except KeyError:
            # do nothing
            pass
        try:
            video["channel_id"] = item["snippet"]["videoOwnerChannelId"]
        except KeyError:
            video["channel_id"] = item["snippet"]["channelId"]
        try:
            video["title"] = item["snippet"]["localized"]["title"]
        except KeyError:
            # do nothing
            pass
        try:
            video["description"] = item["snippet"]["localized"]["description"]
        except KeyError:
            # do nothing
            pass
        try:
            video["tags"] = item["snippet"]["tags"]
        except KeyError:
            # do nothing
            pass
        try:
            video["category_id"] = item["snippet"]["categoryId"]
        except KeyError:
            # do nothing
            pass
        try:
            video["default_language"] = item["snippet"]["defaultAudioLanguage"]
        except KeyError:
            # do nothing
            pass
        try:
            video["duration"] = item["contentDetails"]["duration"]
        except KeyError:
            # do nothing
            pass
        try:
            video["licensed_content"] = item["contentDetails"]["licensedContent"]
        except KeyError:
            # do nothing
            pass
        try:
            video["license"] = item["status"]["license"]
        except KeyError:
            # do nothing
            pass
        try:
            video["public_stats_viewable"] = item["status"]["publicStatsViewable"]
        except KeyError:
            # do nothing
            pass
        try:
            video["made_for_kids"] = item["status"]["madeForKids"]
        except KeyError:
            # do nothing
            pass
        try:
            video["view_count"] = item["statistics"]["viewCount"]
        except KeyError:
            # do nothing
            pass
        try:
            video["like_count"] = item["statistics"]["likeCount"]
        except KeyError:
            # do nothing
            pass
        try:
            video["dislike_count"] = item["statistics"]["dislikeCount"]
        except KeyError:
            # do nothing
            pass
        try:
            video["comment_count"] = item["statistics"]["commentCount"]
        except KeyError:
            # do nothing
            pass
        try:
            video["topic_categories"] = item["topicDetails"]["topicCategories"]
        except KeyError:
            # do nothing
            pass
        videos.append(video)
    return videos
