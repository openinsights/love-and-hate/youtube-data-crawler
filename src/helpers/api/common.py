import numpy as np
from datetime import datetime, date, timezone, timedelta

quota_usage = 0


def get_api_date():
    now_pst = datetime.now(timezone(-timedelta(hours=8)))
    date_iso_str = now_pst.strftime("%Y-%m-%d")
    return date.fromisoformat(date_iso_str)


def increment_quota_count():
    global quota_usage
    quota_usage += 1


def calc_quota_cost(app_conf, mode):
    count = 0
    if mode in ["validate_channels", "update_channels"]:
        for i in range(int(np.ceil(
            app_conf.channel_limit / app_conf.channel_batch_size
        ))):
            # get uploads_id
            count += 1
    elif mode in ["fetch_video_ids", "fetch_video_details"]:
        for i in range(app_conf.channel_limit):
            # get video_ids of uploads
            for n in range(int(np.ceil(
                app_conf.video_limit / app_conf.video_batch_size
            ))):
                # calc pagination
                count += 1
    elif mode == "fetch_video_comments":
        for i in range(app_conf.video_limit):
            # get comment threads of video
            for n in range(int(np.ceil(
                app_conf.comment_limit / app_conf.comment_batch_size
            ))):
                # calc pagination
                count += 1
    return count
