from datetime import datetime, timezone

from helpers.api.common import increment_quota_count


def fetchChannelDetails(youtube, channel_ids):
    request = youtube.channels().list(
        part="snippet,contentDetails,statistics,topicDetails,status,contentOwnerDetails",
        id=channel_ids,
        hl="en_US",
        maxResults=50
    )
    increment_quota_count()
    response = request.execute()
    return response


def append_invalid_channels(channel_ids, channel_objs):
    valid_channel_ids = [channel['channel_id'] for channel in channel_objs]
    invalid_channel_ids = [id for id in channel_ids if id not in valid_channel_ids]
    for id in invalid_channel_ids:
        channel_objs.append({'channel_id': id, 'is_valid': False, 'validated_at': datetime.now(timezone.utc)})

    if len(channel_objs) != len(channel_ids):
        print("ERROR: Batch length is unequal")
        exit()

    return channel_objs


def channel_objs_from_response(response):
    channels = list()
    for item in response["items"]:
        channel = {}
        channel["kind"] = item["kind"]
        channel["etag"] = item["etag"]
        channel["channel_id"] = item["id"]
        channel["title"] = item["snippet"]["title"]
        channel["description"] = item["snippet"]["description"]
        channel["published_at"] = item["snippet"]["publishedAt"]
        try:
            channel["default_language"] = item["snippet"]["defaultLanguage"]
        except KeyError:
            # do nothing when property not in response
            pass
        try:
            channel["localized_title"] = item["snippet"]["localized"]["title"]
        except KeyError:
            # do nothing when property not in response
            pass
        try:
            channel["localized_description"] = item["snippet"]["localized"]["description"]
        except KeyError:
            # do nothing when property not in response
            pass
        try:
            channel["country"] = item["snippet"]["country"]
        except KeyError:
            # do nothing when property not in response
            pass
        try:
            channel["likes_playlist_id"] = item["contentDetails"]["relatedPlaylists"]["likes"]
        except KeyError:
            # do nothing when property not in response
            pass
        try:
            channel["uploads_playlist_id"] = item["contentDetails"]["relatedPlaylists"]["uploads"]
        except KeyError:
            # do nothing when property not in response
            pass
        try:
            channel["view_count"] = item["statistics"]["viewCount"]
        except KeyError:
            # do nothing when property not in response
            pass
        try:
            channel["subscriber_count"] = item["statistics"]["subscriberCount"]
        except KeyError:
            # do nothing when property not in response
            pass
        try:
            channel["video_count"] = item["statistics"]["videoCount"]
        except KeyError:
            # do nothing when property not in response
            pass
        try:
            channel["topic_categories"] = item["topicDetails"]["topicCategories"]
        except KeyError:
            # do nothing when property not in response
            pass
        try:
            channel["made_for_kids"] = item["status"]["madeForKids"]
        except KeyError:
            # do nothing when property not in response
            pass
        try:
            channel["content_owner_id"] = item["contentOwnerDetails"]["contentOwner"]
        except KeyError:
            # do nothing when property not in response
            pass
        channel["is_valid"] = True
        channel["validated_at"] = datetime.now(timezone.utc)
        channels.append(channel)
    return channels
