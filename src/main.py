# -*- coding: utf-8 -*-

# COMMON
import traceback
import routines.fetch.channels as rt_ch
import routines.fetch.videos as rt_vid
import routines.fetch.comments as rt_com
import routines.imports.channels as rt_imp_ch
import routines.imports.videos as rt_imp_vid

# CONFIG
import argparse
import configparser
from helpers import config as hp_conf

# YOUTUBE API
import os
import pickle
import googleapiclient.discovery
import googleapiclient.errors
from google.auth.transport.requests import Request
from google_auth_oauthlib.flow import InstalledAppFlow
from helpers.api import common as hp_acom
from helpers.api import channels as hp_channels
from helpers.api import videos as hp_videos
from helpers.api import comments as hp_comments

# DATABASE
import psycopg2
from helpers import db as hp_db


def main():
    parser = hp_conf.handle_cli_arguments(argparse.ArgumentParser)
    cli_args = parser.parse_args()
    config = configparser.ConfigParser()
    config.read(os.path.dirname(__file__) + '/config.ini')
    cnf = hp_conf.load_app_config(config, cli_args)

    print("♫ Fetch it, bring it, babe, watch it...")

    credentials = None
    if os.path.exists("token.pickle"):
        with open("token.pickle", "rb") as token:
            credentials = pickle.load(token)

    if not credentials or not credentials.valid:
        if credentials and credentials.expired and credentials.refresh_token:
            credentials.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                config['API']['OAUTH_CREDENTIALS_FILE'],
                scopes=["https://www.googleapis.com/auth/youtube.force-ssl"]
            )
            flow.run_console()
            credentials = flow.credentials

            with open("token.pickle", "wb") as file:
                pickle.dump(credentials, file)

    youtube = googleapiclient.discovery.build(
        config['API']['API_SERVICE_NAME'],
        config['API']['API_VERSION'],
        credentials=credentials,
        developerKey=config['API']['YOUTUBE_API_KEY']
    )

    db_conn = psycopg2.connect(
        host=config['DB']['DB_HOST'],
        dbname=config['DB']['DB_NAME'],
        user=config['DB']['DB_USER'],
        password=config['DB']['DB_PASS']
    )

    try:
        if cli_args.validate and cli_args.channels:
            rt_ch.validate_channel_details(cnf, db_conn, youtube, hp_acom, hp_channels, hp_db)

        if cli_args.update and cli_args.channels:
            rt_ch.update_channel_details(cnf, db_conn, youtube, hp_acom, hp_channels, hp_db)

        if cli_args.fetch and cli_args.video_ids:
            rt_vid.fetch_video_ids(cnf, db_conn, youtube, hp_acom, hp_videos, hp_db)

        if cli_args.fetch and cli_args.video_details:
            rt_vid.fetch_video_details(cnf, db_conn, youtube, hp_acom, hp_videos, hp_db)

        if cli_args.fetch and cli_args.comments:
            rt_com.fetch_video_comments(cnf, db_conn, youtube, hp_acom, hp_comments, hp_db)

        if bool(cli_args.import_file) and cli_args.channels:
            rt_imp_ch.import_channels(cli_args.import_file, db_conn, hp_db)

        if bool(cli_args.import_file) and cli_args.videos:
            rt_imp_vid.import_videos(cli_args.import_file, db_conn, hp_db)

    except Exception:
        print()
        traceback.print_exc()

    finally:
        hp_db.insert_quota_usage(db_conn, hp_acom)
        db_conn.close()


if __name__ == "__main__":
    main()
