import numpy as np
import traceback


def fetch_video_ids(app_conf, db_conn, youtube, hp_acom, hp_api, hp_db):
    cost = hp_acom.calc_quota_cost(app_conf, "fetch_video_ids")

    print("fetching video ids")
    print("channel limit: ", app_conf.channel_limit)
    print("videos per channel: ", app_conf.video_limit)
    print("cost: ", cost)
    print("days: ", cost / 10000)
    print("possible runs per day: ", int(10000 / cost))
    db_cur = db_conn.cursor()
    select_query = """
        SELECT ch.channel_id, ch.uploads_playlist_id
        FROM channels AS ch
        LEFT JOIN
        (
            SELECT vid.channel_id, COUNT(vid.channel_id)
            FROM videos AS vid
            GROUP BY vid.channel_id
        ) AS vid_count
        ON ch.channel_id = vid_count.channel_id
        WHERE ch.is_valid IS TRUE
        AND
        (
            (vid_count.count < ch.video_count AND vid_count.count < %s)
            OR vid_count.count IS NULL
        )
        ORDER BY subscriber_count DESC
        LIMIT %s
    """
    db_cur.execute(select_query, (app_conf.video_limit, app_conf.channel_limit,))
    query_response = db_cur.fetchmany(app_conf.channel_limit)
    db_cur.close()

    channel_count = 0
    channels_to_fetch = len(query_response)
    for (channel_id, uploads_id) in query_response:
        last_page = False
        channel_count += 1
        video_count = 0
        print(
            "fetching videos of channel no: ",
            channel_count,
            "/",
            channels_to_fetch,
            end="\r"
        )
        try:
            api_response = hp_api.fetch_playlist_items(youtube, uploads_id)
        except Exception:
            traceback.print_exc()
            continue

        try:
            page_token = api_response["nextPageToken"]
        except KeyError:
            last_page = True
            pass
        max_pages = int(np.ceil(int(api_response["pageInfo"]["totalResults"]) / int(api_response["pageInfo"]["resultsPerPage"])))
        pages_limit = int(np.ceil(app_conf.video_limit / int(api_response["pageInfo"]["resultsPerPage"])))
        pages = min([max_pages, pages_limit])
        playlist_items = hp_api.playlistItem_objs_from_response(api_response)
        for item in playlist_items:
            if video_count < app_conf.video_limit:
                hp_db.insert_video(db_conn, item)
            else:
                break
            video_count += 1
        db_conn.commit()

        if last_page:
            hp_db.update_video_count(db_conn, channel_id)
            continue

        for page in range(pages):
            api_response = hp_api.fetch_playlist_items(youtube, uploads_id, page_token)
            try:
                page_token = api_response["nextPageToken"]
            except KeyError:
                last_page = True
                pass
            playlist_items = hp_api.playlistItem_objs_from_response(api_response)

            for item in playlist_items:
                if video_count < app_conf.video_limit:
                    hp_db.insert_video(db_conn, item)
                else:
                    break
                video_count += 1
            db_conn.commit()

            if last_page:
                hp_db.update_video_count(db_conn, channel_id)
                continue
    print()


def fetch_video_details(app_conf, db_conn, youtube, hp_acom, hp_api, hp_db):
    print("fetching video details")
    cost = hp_acom.calc_quota_cost(app_conf, "fetch_video_details")
    batches_per_channel = int(
        np.ceil(app_conf.video_limit / app_conf.video_batch_size)
    )
    total_batches = batches_per_channel * app_conf.channel_limit

    print("channel limit: ", app_conf.channel_limit)
    print("videos per channel: ", app_conf.video_limit)
    print("batch size: ", app_conf.channel_batch_size)
    print("cost: ", cost)
    print("days: ", cost / 10000)
    print("possible runs per day: ", int(10000 / cost))

    db_cur = db_conn.cursor()
    db_cur.execute("""
        SELECT DISTINCT channel_id
        FROM videos
        WHERE kind = 'youtube#playlistItem'
        LIMIT %s;
    """, (app_conf.channel_limit,))
    query_response = db_cur.fetchmany(app_conf.channel_limit)
    db_cur.close()

    channel_ids = [id for (id,) in query_response]
    if len(channel_ids) == 0:
        exit("all channels in database have been validated!")

    for channel_count, channel_id in enumerate(channel_ids):
        db_cur = db_conn.cursor()
        db_cur.execute("""
            SELECT video_id
            FROM videos
            WHERE channel_id = %s
            AND kind = 'youtube#playlistItem'
            ORDER BY published_at DESC
            LIMIT %s;
        """, (channel_id, app_conf.video_limit,))
        query_response = db_cur.fetchmany(app_conf.video_limit)
        db_cur.close()

        video_ids = [id for (id,) in query_response]
        if len(video_ids) == 0:
            continue

        for batch_count in range(batches_per_channel):
            start = batch_count * app_conf.video_batch_size
            end = (batch_count + 1) * app_conf.video_batch_size
            batch_of_video_ids = video_ids[start:end]
            if len(batch_of_video_ids) == 0:
                continue

            current_batch_no = channel_count * batches_per_channel + batch_count + 1
            print("fetching batch no: ", current_batch_no, "/", total_batches,  end="\r")

            api_response = hp_api.fetchVideoDetails(youtube, batch_of_video_ids)
            video_objs = hp_api.video_objs_from_response(api_response)

            for video in video_objs:
                hp_db.update_video(db_conn, video)

            db_conn.commit()
    print()
