from googleapiclient.errors import HttpError


def fetch_video_comments(app_conf, db_conn, youtube, hp_acom, hp_api, hp_db):
    cost = hp_acom.calc_quota_cost(app_conf, "fetch_video_comments")

    print("fetching comment threads")
    print("video limit: ", app_conf.video_limit)
    print("comments per channel: ", app_conf.comment_limit)
    print("cost: ", cost)
    print("days: ", cost / 10000)
    print("possible runs per day: ", int(10000 / cost))
    db_cur = db_conn.cursor()
    select_query = """
        SELECT vid.video_id, vid.channel_id
        FROM videos AS vid
        LEFT JOIN
        (
            SELECT com.video_id, COUNT(com.video_id)
            FROM comments AS com
            GROUP BY com.video_id
        ) AS com_count
        ON vid.video_id = com_count.video_id
        WHERE
            kind = 'youtube#video'
            AND NOT comment_count = 0
            AND NOT comment_count IS NULL
            AND NOT comments_disabled IS TRUE
            AND
            (
                (com_count.count < vid.comment_count AND com_count.count < %s)
                OR
                com_count.count IS NULL
            )
        ORDER BY view_count DESC
        LIMIT %s;
    """
    db_cur.execute(select_query, (app_conf.comment_limit, app_conf.video_limit,))
    query_response = db_cur.fetchmany(app_conf.video_limit)
    db_cur.close()

    video_count = 0
    videos_to_fetch = len(query_response)
    for (video_id, channel_id) in query_response:
        last_page = False
        video_count += 1
        comment_count = 0
        print(
            "fetching comments of video no: ",
            video_count,
            "/",
            videos_to_fetch,
            end="\r"
        )
        try:
            api_response = hp_api.fetch_comment_threads(youtube, video_id)
        except HttpError as error:
            reason = error.error_details[0]["reason"]
            if reason == "commentsDisabled":
                hp_db.update_comments_disabled(db_conn, video_id, True)
                continue
            else:
                raise

        try:
            page_token = api_response["nextPageToken"]
        except KeyError:
            last_page = True
            pass

        comment_threads = hp_api.comment_thread_objs_from_response(api_response)
        for comment in comment_threads:
            comment["channel_id"] = channel_id

            if comment_count < app_conf.comment_limit:
                hp_db.insert_comment(db_conn, comment)
            else:
                break
            comment_count += 1
        db_conn.commit()

        if last_page:
            hp_db.update_comment_count(db_conn, video_id)
            continue

        while not last_page:
            try:
                api_response = hp_api.fetch_comment_threads(youtube, video_id, page_token)
            except HttpError as error:
                reason = error.error_details[0]["reason"]
                if reason == "commentsDisabled":
                    hp_db.update_comments_disabled(db_conn, video_id, True)
                    continue
                else:
                    raise
            try:
                page_token = api_response["nextPageToken"]
            except KeyError:
                last_page = True
                pass
            comment_threads = hp_api.comment_thread_objs_from_response(api_response)

            for comment in comment_threads:
                comment["channel_id"] = channel_id

                if comment_count < app_conf.comment_limit:
                    hp_db.insert_comment(db_conn, comment)
                else:
                    break
                comment_count += 1
            db_conn.commit()

            if last_page:
                hp_db.update_comment_count(db_conn, video_id)
                continue
    print()
