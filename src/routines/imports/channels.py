import pandas as pd
import numpy as np


def import_channels(path, db_conn, helpers):
    try:
        df = pd.read_csv(path, delimiter=";", dtype=str)
    except:
        msg = "ERROR: could not read from file: \"" + path + "\""
        exit(msg)

    total_rows = df.shape[0]
    df = df.replace({np.nan: None})

    print("importing channels from", path)
    for i in df.index:
        print("importing row: ", i, "/", total_rows, end="\r")
        query = """INSERT INTO channels (
            kind,
            etag,
            channel_id,
            title,
            description,
            published_at,
            default_language,
            localized_title,
            localized_description,
            country,
            likes_playlist_id,
            uploads_playlist_id,
            view_count,
            subscriber_count,
            video_count,
            topic_categories,
            made_for_kids,
            content_owner_id,
            is_valid,
            validated_at,
            invalid_uploads_id,
            created_at,
            updated_at
            ) VALUES (
            %s,%s,%s,%s,%s,%s,%s,%s,
            %s,%s,%s,%s,%s,%s,%s,%s,
            %s,%s,%s,%s,%s,%s,%s);
        """

        data = (
            df.loc[i, 'kind'],
            df.loc[i, 'etag'],
            df.loc[i, 'channel_id'],
            df.loc[i, 'title'],
            df.loc[i, 'description'],
            df.loc[i, 'published_at'],
            df.loc[i, 'default_language'],
            df.loc[i, 'localized_title'],
            df.loc[i, 'localized_description'],
            df.loc[i, 'country'],
            df.loc[i, 'likes_playlist_id'],
            df.loc[i, 'uploads_playlist_id'],
            df.loc[i, 'view_count'],
            int(df.loc[i, 'subscriber_count']),
            int(df.loc[i, 'video_count']),
            df.loc[i, 'topic_categories'],
            bool(df.loc[i, 'made_for_kids']),
            df.loc[i, 'content_owner_id'],
            bool(df.loc[i, 'is_valid']),
            df.loc[i, 'validated_at'],
            bool(df.loc[i, 'invalid_uploads_id']),
            df.loc[i, 'created_at'],
            df.loc[i, 'updated_at']
        )

        helpers.insert_row(db_conn, query, data)
    print()
