CREATE TABLE channels (
  kind varchar(255),
  etag varchar(255),
  channel_id varchar(255) NOT NULL,
  title varchar(255),
  description text,
  published_at timestamp(0),
  default_language varchar(255),
  localized_title varchar(255),
  localized_description text,
  country varchar(255),
  likes_playlist_id varchar(255),
  uploads_playlist_id varchar(255),
  view_count bigint,
  subscriber_count bigint,
  video_count bigint,
  topic_categories text[],
  made_for_kids boolean,
  content_owner_id varchar(255),
  is_valid boolean,
  validated_at timestamp(0),
  invalid_uploads_id boolean,
  created_at timestamp(0) NOT NULL,
  updated_at timestamp(0) NOT NULL,
  PRIMARY KEY (channel_id)
);

CREATE TABLE videos (
  kind varchar(255),
  etag varchar(255),
  video_id varchar(255) NOT NULL,
  published_at timestamp(0),
  channel_id varchar(255) NOT NULL,
  title varchar(255),
  description text,
  tags varchar(255)[],
  category_id varchar(255),
  default_language varchar(255),
  duration varchar(255),
  licensed_content boolean,
  content_rating text,
  license varchar(255),
  public_stats_viewable boolean,
  made_for_kids boolean,
  view_count bigint,
  like_count bigint,
  dislike_count bigint,
  comment_count bigint,
  comments_disabled boolean,
  topic_categories text[],
  created_at timestamp(0) NOT NULL,
  updated_at timestamp(0) NOT NULL,
  PRIMARY KEY (video_id)
);

CREATE TABLE video_category (
  kind varchar(255),
  etag varchar(255),
  category_id varchar(255) NOT NULL,
  created_by varchar(255),
  title varchar(255),
  assignable boolean,
  created_at timestamp(0) NOT NULL,
  updated_at timestamp(0) NOT NULL,
  PRIMARY KEY (category_id)
);

CREATE TABLE comments (
  comment_id varchar(255) NOT NULL,
  channel_id varchar(255) NOT NULL,
  video_id varchar(255) NOT NULL,
  comment_thread_etag varchar(255),
  comment_thread_id varchar(255),
  reply_count integer,
  author_display_name varchar(255),
  author_channel_id varchar(255),
  text_display text,
  text_original text,
  parent_id varchar(255),
  like_count integer,
  published_at timestamp(0),
  edited_at timestamp(0),
  created_at timestamp(0) NOT NULL,
  updated_at timestamp(0) NOT NULL,
  PRIMARY KEY (comment_id)
);

CREATE TABLE quota_usage (
  id serial,
  api_date date NOT NULL,
  quotas integer NOT NULL,
  PRIMARY KEY(id),
  UNIQUE (api_date)
);

CREATE TABLE bookmarks (
  id serial,
  channel_id varchar(255) NOT NULL REFERENCES channels (channel_id),
  video_id varchar(255) NOT NULL REFERENCES videos (video_id),
  date timestamp(0),
  PRIMARY KEY (id)
);