FROM continuumio/miniconda3

WORKDIR /usr/src/code

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt
RUN echo "PS1='🐳 \[\033[1;36m\]python-dev \[\033[1;34m\]\w\[\033[0;35m\] \[\033[1;36m\]# \[\033[0m\]'" >> /root/.bashrc
