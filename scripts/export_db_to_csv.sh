#!/bin/bash

psql -h localhost -U user -d lovehate -c "COPY (SELECT * FROM channels) TO '/exports/channels.csv' DELIMITER ';' CSV HEADER;"
psql -h localhost -U user -d lovehate -c "COPY (SELECT * FROM videos) TO '/exports/videos.csv' DELIMITER ';' CSV HEADER;"
