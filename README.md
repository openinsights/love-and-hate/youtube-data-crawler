# Youtube Data Collector

A cli tool to collect youtube data on channels -> videos -> comments based on a list of channel IDs.

## Prerequisites

- [Docker Compose](https://docs.docker.com/compose/install/)
- [Google Credentials](https://developers.google.com/youtube/registering_an_application)
- A list of youtube channel IDs stored in the database

## Set up

```bash
# build docker environment
docker-compose build

# launch docker environment
docker-compose up -d --remove-orphans

# enter python dev container
docker-compose exec python bash
```

## Configuration
1. create local config
    ```bash
    cp config.ini.example config.ini
    ```
2. save google oauth desktop app credentials file in project root
3. fill with respective values

App parameters can be overwritten temporarily when running the program. See [Usage section](#Usage) for more details.

## Usage

```bash
# show help
python src/main.py --help

# import data from csv
python src/main.py --channels --import datasets/channels.csv
python src/main.py --videos --import datasets/videos.csv

# validate channel data
python src/main.py --validate --channels

# update channel data
python src/main.py --update --channels

# fetch 100 video ids for 10 channels
python src/main.py --fetch --video-ids --channel-limit 10 --video-limit 100

# fetch video data of 100 video ids for 10 channels
python src/main.py --fetch --video-details --channel-limit 10 --video-limit 100

# fetch 1000 comment threads for 100 videos
python src/main.py --fetch --comments --video-limit 100 --comment-limit 1000
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.
